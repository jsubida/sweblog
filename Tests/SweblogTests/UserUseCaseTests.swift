//
//  UserUseCaseTests.swift
//  
//
//  Created by J.C. Subida on 9/13/23.
//

import XCTest
@testable import Sweblog

final class UserUseCaseTests: XCTestCase {

    private var useCase: UserUseCase!
    
    override func setUpWithError() throws {
        useCase = UserUseCase(port: InMemoryUserAdapter())
    }

    func testCreateUser() throws {
        let uname = "iAmUser"
        let id = UUID()
        let user = useCase.create(username: uname, id: id)
        XCTAssertEqual(user.username, uname)
        XCTAssertEqual(user.id, id)
    }

    func testFetchById() throws {
        let uname = "iAmUser"
        let id = UUID()
        let user = useCase.create(username: uname, id: id)
        
        let fetched = useCase.fetchById(id)
        
        XCTAssertEqual(user.id, fetched?.id)
    }
}
