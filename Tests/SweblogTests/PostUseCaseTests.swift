//
//  PostUseCaseTests.swift
//  
//
//  Created by J.C. Subida on 9/13/23.
//

import XCTest
@testable import Sweblog

final class PostUseCaseTests: XCTestCase {

    private var useCase: PostUseCase!
    
    override func setUpWithError() throws {
        useCase = PostUseCase(port: InMemoryPostAdapter())
    }
    
    func testCreatePost() {
        let title = "Title"
        let contents = "Contents"
        let authorId = UUID()
        let slug = "title"
        let id = UUID()
        
        let post = useCase.create(title: title, contents: contents, author: authorId, slugUrl: slug, id: id)
        
        XCTAssertEqual(post.title, title)
        XCTAssertEqual(post.contents, contents)
        XCTAssertEqual(post.authorId, authorId)
        XCTAssertEqual(post.slugUrl, slug)
        XCTAssertEqual(post.id, id)
    }
    
    func testFetchById() {
        let title = "Title"
        let contents = "Contents"
        let authorId = UUID()
        let slug = "title"
        let id = UUID()
        let created = useCase.create(title: title, contents: contents, author: authorId, slugUrl: slug, id: id)
        
        let fetched = useCase.fetchById(id)
        
        XCTAssertNotNil(fetched)
        XCTAssertEqual(created.title, fetched?.title)
        XCTAssertEqual(created.contents, fetched?.contents)
        XCTAssertEqual(created.authorId, fetched?.authorId)
        XCTAssertEqual(created.slugUrl, fetched?.slugUrl)
        XCTAssertEqual(created.id, fetched?.id)
    }
    
    func testFetchForAuthor() {
        let title = "Title"
        let contents = "Contents"
        let authorId = UUID()
        let slug = "title"
        let id = UUID()
        let created = useCase.create(title: title, contents: contents, author: authorId, slugUrl: slug, id: id)
        
        let posts = useCase.fetchForAuthor(authorId)
        
        XCTAssertEqual(posts.count, 1)
        let post = posts[0]
        XCTAssertEqual(created.title, post.title)
        XCTAssertEqual(created.contents, post.contents)
        XCTAssertEqual(created.authorId, post.authorId)
        XCTAssertEqual(created.slugUrl, post.slugUrl)
        XCTAssertEqual(created.id, post.id)
    }
}
