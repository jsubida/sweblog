# Sweblog

[![Swift](https://img.shields.io/badge/Swift-5.8_5.9-blue?style=flat-square)](https://img.shields.io/badge/Swift-5.8_5.9-blue?style=flat-square)
[![Platforms](https://img.shields.io/badge/Platforms-macOS_iOS-yellow?style=flat-square)](https://img.shields.io/badge/Platforms-macOS_iOS-yellow?style=flat-square)
[![Swift Package Manager](https://img.shields.io/badge/Swift_Package_Manager-compatible-blue?style=flat-square)](https://img.shields.io/badge/Swift_Package_Manager-compatible-blue?style=flat-square)

Sweblog is a Swift package to facilitate blog implementations. BYO persistence and interface.

## Usage

This package is an exercise in implementing [hexagonal architecture](https://en.wikipedia.org/wiki/Hexagonal_architecture_(software)). Applications should create adapters for their persistence mechanism and utilize use cases for interacting with entities.  
