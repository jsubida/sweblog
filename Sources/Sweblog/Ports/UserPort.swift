//
//  UserPort.swift
//  
//
//  Created by J.C. Subida on 9/13/23.
//

import Foundation

/// The type defining the methods to access the `Userable` entity type.
public protocol UserPort {
    func create(username: String, id: UUID?) -> any Userable
    func fetchById(_ id: UUID) -> (any Userable)?
}
