//
//  PostPort.swift
//  
//
//  Created by J.C. Subida on 9/13/23.
//

import Foundation

/// The type defining methods for accessing `Postable` types.
public protocol PostPort {
    func create(title: String, contents: String, author: UUID, slugUrl: String, id: UUID?) -> any Postable
    func fetchById(_ id: UUID) -> (any Postable)?
    func fetchForAuthor(_ authorId: UUID) -> [any Postable]
}
