//
//  PostUseCase.swift
//  
//
//  Created by J.C. Subida on 9/13/23.
//

import Foundation

/// The type for interacting with `Postable` types
public protocol PostUseCaseProtocol {
    func create(title: String, contents: String, author: UUID, slugUrl: String, id: UUID?) -> any Postable
    func fetchById(_ id: UUID) -> (any Postable)?
    func fetchForAuthor(_ authorId: UUID) -> [any Postable]
}

public class PostUseCase {
    private let repo: PostPort
    
    public init(port: PostPort) {
        repo = port
    }
    
    func create(title: String, contents: String, author: UUID, slugUrl: String, id: UUID?) -> any Postable {
        repo.create(title: title, contents: contents, author: author, slugUrl: slugUrl, id: id)
    }
    
    func fetchById(_ id: UUID) -> (any Postable)? {
        repo.fetchById(id)
    }
    
    func fetchForAuthor(_ authorId: UUID) -> [any Postable] {
        repo.fetchForAuthor(authorId)
    }
}
