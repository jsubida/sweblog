//
//  UserUseCase.swift
//  
//
//  Created by J.C. Subida on 9/13/23.
//

import Foundation

/// The type defining methods for interacting with `Userable` types
public protocol UserUseCaseProtocol {
    func create(username: String, id: UUID?) -> any Userable
    func fetchById(_ id: UUID) -> (any Userable)?
}

public class UserUseCase: UserUseCaseProtocol {
    
    private let repo: UserPort
    
    public init(port: UserPort) {
        repo = port
    }
    
    public func create(username: String, id: UUID?) -> any Userable {
        repo.create(username: username, id: id)
    }
    
    public func fetchById(_ id: UUID) -> (any Userable)? {
        repo.fetchById(id)
    }
}
