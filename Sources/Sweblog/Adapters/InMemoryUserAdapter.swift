//
//  InMemoryUserAdapter.swift
//  
//
//  Created by J.C. Subida on 9/13/23.
//

import Foundation

public class InMemoryUserAdapter: UserPort {
    private var users: [any Userable] = []
    
    public func create(username: String, id: UUID?) -> any Userable {
        let user = User(id: id ?? UUID(), username: username)
        users.append(user)
        return user
    }
    
    public func fetchById(_ id: UUID) -> (any Userable)? {
        users.first(where: { $0.id == id})
    }
}
