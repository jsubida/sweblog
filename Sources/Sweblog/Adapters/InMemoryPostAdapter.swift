//
//  InMemoryPostAdapter.swift
//  
//
//  Created by J.C. Subida on 9/13/23.
//

import Foundation

public class InMemoryPostAdapter: PostPort {
    private var posts: [any Postable] = []
    
    public func create(title: String, contents: String, author: UUID, slugUrl: String, id: UUID?) -> any Postable {
        let now = Date()
        let post = Post(id: id ?? UUID(), title: title, contents: contents, authorId: author, createdAt: now, updatedAt: now, slugUrl: slugUrl)
        posts.append(post)
        return post
    }
    
    public func fetchById(_ id: UUID) -> (any Postable)? {
        posts.first(where: {$0.id == id})
    }
    
    public func fetchForAuthor(_ authorId: UUID) -> [any Postable] {
        posts.filter { post in
            post.authorId == authorId
        }
    }
}
