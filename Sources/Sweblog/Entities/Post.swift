//
//  Post.swift
//  
//
//  Created by J.C. Subida on 9/13/23.
//

import Foundation

/// A type that can represent an online message
public protocol Postable: Codable {
    var id: UUID? { get }
    
    var title: String { get set }
    
    var contents: String { get set }
    
    var authorId: UUID { get }
    
    var createdAt: Date? { get }
    
    var updatedAt: Date? { get set }
    
    var slugUrl: String { get set }
}

/// A `Post` is an online message
public struct Post: Postable {
    public var id: UUID?
    
    public var title: String
    
    public var contents: String
    
    public var authorId: UUID
    
    public var createdAt: Date?
    
    public var updatedAt: Date?
    
    public var slugUrl: String
}
