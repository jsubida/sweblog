//
//  User.swift
//  
//
//  Created by J.C. Subida on 9/13/23.
//

import Foundation

/// A type that represents a single user
public protocol Userable: Codable {
    var id: UUID? { get }
    
    var username: String? { get set }
}

public struct User: Userable {
    public var id: UUID?
    
    public var username: String?
}
